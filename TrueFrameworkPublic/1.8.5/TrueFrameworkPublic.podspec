#
# Be sure to run `pod lib lint UserPanelSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'TrueFrameworkPublic'
s.version          = '1.8.5'
s.summary          = 'True SDK Login for swift 5.3'

s.description      = <<-DESC
TODO: Add long description of the pod here.
version 0.1.8
-- Added
- Print log when application using SDKV.4 that operating.
-- Fixed Bug
- Application is log in after Application using SDK V.3 that
update to SDK v.4
- TrueId Application is loged in after Application using SDK V.3 that
update to SDK v.4
- Applicatin hide progress bar after forget password success for Ipad Device.=
DESC

s.homepage         = 'https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'willshere' => 'kowit_nan@truecorp.cp.th' }
s.source           = { :git => 'https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '9.0'

s.ios.vendored_frameworks = "TrueFrameworkPublic/Framework/TrueIDFramework.xcframework"
#s.source_files = 'TrueIDFrameworkPublic/Classes/**/*'

# s.resource_bundles = {
#   'TrueIDFrameworkPublic' => ['TrueIDFrameworkPublic/Assets/*.png']
# }

# s.public_header_files = 'Pod/Classes/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'
# s.dependency 'AFNetworking', '~> 2.3'

s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end

