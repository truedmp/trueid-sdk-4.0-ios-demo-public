# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 5.3.0 (2024-03-12)

### Features
* Support Xcode15 ([2184c5c](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios/commit/4af2eabf5c6e61a8fa941cbaa52489b20a8b491a))

### Bug Fixes
[LS-14769] remove deleteAccessToken2stExpireCache from clearSDKCache ([9d30bba](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios/commit/9d30bbabc7bcceb0fa35ccf5c62b94f11c938b85))

## 5.2.2 (2023-10-21)

* [OPEN-3260] add onRefreshTokenRevoke delegate ([8ecc42a](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/8ecc42ae20dcb9b8d12cd38af80b99eb6bc3dfcc))

## 5.2.0 (2022-12-08)
* [OPEN-1922] add handleJavaScript ([942118d](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/942118da0ce23fa4fe84334f630be719ccb77f5f))

### Features

* [OPEN-1488]Refactoring: TrueID SDK ([0629589](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/062958974bc485fa1c5ade0fe39c17b484d2def2))

## 5.1.0 (2022-12-08)
* [OPEN-1238] Support biometric
* Support Social login

### 5.0.2 (2022-12-08)


### Features

* 5.0.2 deploy for testing Moya only ([fcc15dd](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/fcc15dd6f323a72784c902cceb25b2ec703010b5))

### 5.0.1 (2022-12-08)


### Features

* add dSYM ([9cfd444](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/9cfd444405a7000f09a8ce9d7f89c33847236ac8))
* add support language ([b9b3fcc](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/b9b3fcc414c2b3b66bba2fca9d40e1de866316db))
* build target to iOS 9.0 ([5372cc1](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/5372cc11fe761950defdef54d0d5634b2154bcd2))
* bump last version login v4 ([b817364](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/b8173642294ae600ab3a1e7c30d79c6cca5d74e5))
* bump version ([0c2044e](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/0c2044eb2a46da0253d911731a67a9cc9411d311))
* update 1.9.1 ([fcd6229](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/fcd6229716a7445fe0393140aa433f75418026b4))
* update pod version to 1.9.0 ([609f664](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/609f6649edd2936ed541ab609251ff76c2a428e6))
* update SDK ([a757adb](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/a757adb27443b46e711fa18f507cc898e18aa2d5))
* update v 1.9.0 ([2930c28](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/2930c2855f50a7019435b68611e5bff0772bfb8f))
* update version 1.9.7 ([9b87581](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/9b8758183234da2aabfc8747f1e153bded01a787))


### Bug Fixes

* [OPEN-922] fix force logout by device ([e0cad41](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/e0cad4110beda92f8b0d734643b4709d355f7547))
* bug force logout ([24aa517](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/24aa517abfdee97f1b251637b8a2e8c12cfd7a4e))
* bug force logout ([a964fea](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/a964fea08e565141b1e24fb724e2748ba4d2d2b9))
* remove func remove all token ([992f3a1](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/992f3a1ca629852570308cbf894da7f2e399412e))
* remove readme and license ([e56e84e](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/e56e84e1a933bfca1048ffe238cb4032c6d023b5))
* version framework 1.9.6 ([70fd561](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/70fd561121f5fa1d2588c4cb6dfe44a261c0d609))

 
## Version 5.0.0
 - Support Xcode 13.2.1
 - Add Easy login
 - Add Localized Thai, English, Indonesian, Myanmar, Cambodia, Vietnam, Philippines
 - Fixed bug token expire
 - Fixed bug force logout
 - Remove root token.

### 1.9.14 (2022-12-08)

### Bug Fixes

* bug force logout ([24aa517](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/24aa517abfdee97f1b251637b8a2e8c12cfd7a4e))

### 1.9.9 (2022-12-08)

### Bug Fixes

* remove readme and license ([e56e84e](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/e56e84e1a933bfca1048ffe238cb4032c6d023b5))

### 1.9.8 (2022-12-08)


### Features
* add support language ([b9b3fcc](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/b9b3fcc414c2b3b66bba2fca9d40e1de866316db))
* update version 1.9.7 ([9b87581](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/9b8758183234da2aabfc8747f1e153bded01a787))


### Bug Fixes

* version framework 1.9.6 ([70fd561](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/70fd561121f5fa1d2588c4cb6dfe44a261c0d609))

### 1.9.5 (2022-12-08)


### Features

* add dSYM ([9cfd444](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/9cfd444405a7000f09a8ce9d7f89c33847236ac8))
* build target to iOS 9.0 ([5372cc1](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/5372cc11fe761950defdef54d0d5634b2154bcd2))
* bump last version login v4 ([b817364](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/b8173642294ae600ab3a1e7c30d79c6cca5d74e5))
* update 1.9.1 ([fcd6229](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/fcd6229716a7445fe0393140aa433f75418026b4))
* update pod version to 1.9.0 ([609f664](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/609f6649edd2936ed541ab609251ff76c2a428e6))
* update v 1.9.0 ([2930c28](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/2930c2855f50a7019435b68611e5bff0772bfb8f))

### 1.8.5 (2022-12-08)


### Features

* bump last version login v4 ([b817364](https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public/commit/b8173642294ae600ab3a1e7c30d79c6cca5d74e5))
