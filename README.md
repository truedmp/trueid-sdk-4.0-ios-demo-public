# TrueFrameworkPublic

[![CI Status](https://img.shields.io/travis/kowit-will/TrueFrameworkPublic.svg?style=flat)](https://travis-ci.org/kowit-will/TrueFrameworkPublic)
[![Version](https://img.shields.io/cocoapods/v/TrueFrameworkPublic.svg?style=flat)](https://cocoapods.org/pods/TrueFrameworkPublic)
[![License](https://img.shields.io/cocoapods/l/TrueFrameworkPublic.svg?style=flat)](https://cocoapods.org/pods/TrueFrameworkPublic)
[![Platform](https://img.shields.io/cocoapods/p/TrueFrameworkPublic.svg?style=flat)](https://cocoapods.org/pods/TrueFrameworkPublic)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
Support Xcode 13.1 and Swift 5.1

## Installation

TrueFrameworkPublic is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TrueFrameworkPublic'
```

## Author

TrueID Team

## License

TrueFrameworkPublic is available under the MIT license. See the LICENSE file for more info.
